const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')


const app = express()
app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.json({limit: "30mb", extended: false}))
app.use(bodyParser.urlencoded({limit: "30mb", extended: false}))
app.use(cors())

const PORT = 3000

// This will only work if you are a Instagram Business Account

// Before we can use this webhook, we must verify it on
// Meta Developers: https://developers.facebook.com/ 
// You must create a verify token and then
// they will send you a JSON object, from that object,
// we need the req.query.[hub.challenge]

// Also notice that we need to have a secure HTTPS server to be
// able to use these instagram webhook properties

// This endpoint is used to verify our webhook, we need to return the hub.challenge to Facebook
app.get('/instagram', async (req, res) => {
    const hubChallenge = req.query['hub.challenge'] // the token Meta Developer sends us
    const verifyToken = req.query['hub.verify_token'] // the token we created
    res.status(200).send(hubChallenge)
})

// returns incoming message from Instagram
app.post('/instagram', async (req, res) => {
    // const data = req.body.entry[0].changes[0] // this will return the entire message JSON for comments and tags
    const data = req.body.entry[0].messaging // sends message
    console.log(data)
})

// this endpoint is used to verify our webhook, wee need to return the hub.challenge to Facebook
app.get('/messenger', async (req, res) => {
    const hubChallenge = req.query['hub.challenge'] // the token Meta Developer sends us
    const verifyToken = req.query['hub.verify_token'] // the token we created
    res.status(200).send(hubChallenge)
})

// returns incoming message from Messenger
app.post('/messenger', async (req, res) => {
    // this will give you the entire messaging JSON
    // sender, timestamp, message, text, etc
    const data = req.body.entry[0].messaging
    console.log(data)
})

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`)
})